//
//  UIColorExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 2/5/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    // -------------------------------------------------------------------------------
    // MARK: - Colors for project
    
    /// The light gray text color
    static var grayBackgroundColor: UIColor? {
        get {
            return UIColor(hex: "F1F0EB")
        }
    }
    
    static var blueThemeColor: UIColor? {
        get {
            return UIColor(hex: "3F51B5")
        }
    }
    
    // -------------------------------------------------------------------------------
    // MARK: - Utils
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = NSScanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}