//
//  UITableViewExtension.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    class func rowHeight() -> CGFloat {
        return 44
    }
    
    class func nib() -> UINib {
        
        let name =  NSStringFromClass(self).componentsSeparatedByString(".").last!
        return UINib.init(nibName: name, bundle: nil)
    }
}
