//
//  NSErrorExtensions.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/15/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation

extension NSError {
    
    /**
     Initialize an error with In_App_Error, code 12121212, and a localized description
     
     - parameter description: NSLocalizedDescriontopn of the error
     
     - returns: Error Object
     */
    class func errorWithLocalizedDescription(localizedDescription description: String) -> NSError {
        return NSError(domain: "In_App_Error", code: 12121212, userInfo: [NSLocalizedDescriptionKey : description])
    }
}