//
//  Challenge.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class Challenge: NSObject {

    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Mappings
    
    enum MappingKeys: String {
        case idKey = "id"
        case authorIdKey = "authorId"
        case titleKey = "title"
        case descriptionKey = "description"
        case photoKey = "photo"
        case locationNameKey = "locationName"
        case longitudeKey = "longitude"
        case latitudeKey = "latitude"
        case peopleJoinedKey = "peopleJoined"
        case peopleCompletedKey = "peopleCompleted"
        case peopleAprovedKey = "peopleAproved"
    }
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Proprietes
    
    var id: Int?
    var authorId: Int?
    var title: String?
    var descr: String?
    var photoURL: NSURL?
    var locationName: String?
    var longitude: Double?
    var latitude: Double?
    var peopleJoined = [Int]()
    var peopleCompleted = [Int]()
    var peopleAproved = [Int]()
    
    var image: String?
    var type: String?
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Initialisation
    
    override init() {
        super.init()
    }
    
    required init(jsonDictionary: NSDictionary) {
        
        self.id = jsonDictionary[MappingKeys.idKey.rawValue] as? Int
        self.authorId = jsonDictionary[MappingKeys.authorIdKey.rawValue] as? Int
        self.title = jsonDictionary[MappingKeys.titleKey.rawValue] as? String
        self.descr = jsonDictionary[MappingKeys.descriptionKey.rawValue] as? String
        
        if let url = jsonDictionary[MappingKeys.photoKey.rawValue] as? String {
            self.photoURL = NSURL(string: url)
        }
        
       self.locationName = jsonDictionary[MappingKeys.locationNameKey.rawValue] as? String
        self.longitude = jsonDictionary[MappingKeys.longitudeKey.rawValue] as? Double
        self.latitude = jsonDictionary[MappingKeys.latitudeKey.rawValue] as? Double
        
        if let peopleJoined = jsonDictionary[MappingKeys.peopleJoinedKey.rawValue] as? [Int] {
            
            self.peopleJoined = [Int]()
            
            for item in peopleJoined {
                self.peopleJoined.append(item)
            }
        }
        
        if let peopleCompleted = jsonDictionary[MappingKeys.peopleCompletedKey.rawValue] as? [Int] {
            
            self.peopleCompleted = [Int]()
            
            for item in peopleCompleted {
                self.peopleCompleted.append(item)
            }
        }
        
        if let peopleAproved = jsonDictionary[MappingKeys.peopleAprovedKey.rawValue] as? [Int] {
            
            self.peopleAproved = [Int]()
            
            for item in peopleAproved {
                self.peopleAproved.append(item)
            }
        }
    }
    
    
    class  func getAllChallengesFromArray(array: [NSDictionary]) -> [Challenge] {
        
        var result = [Challenge]()
        
        for item in array {
            let challenge = Challenge(jsonDictionary: item)
            result.append(challenge)
        }
        
        return result
    }
}
