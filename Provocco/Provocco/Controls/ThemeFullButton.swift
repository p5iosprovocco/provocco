//
//  ThemeFullButton.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class ThemeFullButton: UIButton {

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.layer.cornerRadius = 3.0
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.redColor()
        self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        self.layer.borderColor = UIColor.blueThemeColor?.CGColor
        self.layer.borderWidth = 1.0
    }

}
