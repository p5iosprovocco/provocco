//
//  LoginViewController.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Outlets
    
    @IBOutlet weak var facebookLoginButton: UIButton! {
        didSet {
            self.facebookLoginButton.layer.cornerRadius = 3.0
            self.facebookLoginButton.layer.masksToBounds = true
        }
    }
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Initialisation
    
    class func loadFromNib() -> LoginViewController {
        let viewController = LoginViewController(nibName: String(LoginViewController), bundle: nil)
        
        return viewController
    }
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Actions
    
    @IBAction private func loginWithFacebookAction(sender: UIButton?) {
        
//        let tutorialViewController = TutorialViewController.loadFromNib()
//        tutorialViewController.modalTransitionStyle = .PartialCurl
//        self.presentViewController(tutorialViewController, animated: true, completion: nil)
//        return
        
        AuthentificationManager.sharedInstance.loginUserWithFacebook(
            forViewController: self,
            succesHandler: { (user, originalResult) in
                
                print(originalResult)
                
                let tutorialViewController = TutorialViewController.loadFromNib()
                tutorialViewController.modalTransitionStyle = .PartialCurl
                self.presentViewController(tutorialViewController, animated: true, completion: nil)
            },
            failureHandler: { (error) in
                
                self.presentErrorAlertViewController(alertMessage: error.localizedDescription)
            }) { 
                // Hide the activity indicators if you use some of them
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
