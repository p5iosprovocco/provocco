//
//  FiltersViewController.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

enum FilterBy: Int {
    case Date
    case Popularity
    case Media
    case Challenge
    case Location
    case None
}

protocol FiltersViewControllerDelegate {
    func pressOK(sender: FiltersViewController, filter: FilterBy)
    func pressCancel(sender: FiltersViewController)
}

class FiltersViewController: UIViewController {

    //---------------------------------------------------------------
    //MARK: Constants
    
    //---------------------------------------------------------------
    //MARK: IBOutlet

    //---------------------------------------------------------------
    //MARK: Parameters
    
    var delegate:FiltersViewControllerDelegate?
    
    //---------------------------------------------------------------
    //MARK: Main functions

    class func loadFromNib() -> FiltersViewController {
        let viewController = FiltersViewController(nibName: String(FiltersViewController), bundle: nil)
        
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Customize the popup
        self.view.layer.cornerRadius = 5
        self.view.layer.masksToBounds = true
        self.view.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    //---------------------------------------------------------------
    //MARK: IBActions
    
    @IBAction func didPressedCancelButton(sender: UIButton) {
        
        if let delegate = self.delegate {
            delegate.pressCancel(self)
        }
    }
    
    @IBAction func didPressedFilterButton(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.delegate?.pressOK(self, filter: .Date)
        break;
            
        case 1:
            self.delegate?.pressOK(self, filter: .Popularity)
        break;
            
        case 2:
            self.delegate?.pressOK(self, filter: .Media)
        break;
            
        case 3:
            self.delegate?.pressOK(self, filter: .Challenge)
        break;
            
        case 4:
            self.delegate?.pressOK(self, filter: .Location)
        break;
            
        case 5:
            self.delegate?.pressOK(self, filter: .None)
        break;
            
        default:
        break;
        }
    }
}
