//
//  ChallengeListViewCell.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit
import Kingfisher

class ChallengeListViewCell: UITableViewCell {

    //---------------------------------------------------------------
    //MARK: Constants
    
    @IBOutlet var photoImageView:UIImageView!{
        didSet {
            self.photoImageView.layer.cornerRadius = self.photoImageView.frame.size.width / 2
            self.photoImageView.clipsToBounds = true
        }
    }
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var peopleJoinedLabel: UILabel!
    
    //---------------------------------------------------------------
    //MARK: IBOutlet

    //---------------------------------------------------------------
    //MARK: Variables
    
    //---------------------------------------------------------------
    //MARK: Main Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithChallenge(challenge: Challenge) {
        
        self.titleLabel.text = challenge.title
        self.descriptionLabel.text = challenge.descr
        
        if let location = challenge.locationName {
            self.locationLabel.text = "Location: \(location)"
        }
        else {
            self.locationLabel.text = "No Location"
        }
        
        self.peopleJoinedLabel.text = "People joined: \(challenge.peopleJoined!.count)"
        
        if let imageURL = challenge.photoURL {
            self.photoImageView.kf_setImageWithURL(imageURL, placeholderImage: UIImage(named: "img-default"))
        }
    }
}
