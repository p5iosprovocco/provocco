//
//  PeopleJoinedCell.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class PeopleJoinedCell: UITableViewCell {

    //---------------------------------------------------------------
    //MARK: Constants
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView! {
        didSet {
            self.photoImageView.layer.cornerRadius = self.photoImageView.frame.size.width / 2
            self.photoImageView.clipsToBounds = true
        }
    }
    
    //---------------------------------------------------------------
    //MARK: IBOutlet
    
    //---------------------------------------------------------------
    //MARK: Variables
    
    //---------------------------------------------------------------
    //MARK: Main Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
