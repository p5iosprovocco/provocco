//
//  TutorialPageViewController.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/3/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class TutorialPageViewController: UIViewController {
    
    
    var titleString: String?
    var logoImage: UIImage?
    
    @IBOutlet weak var logo: UIImageView! {
        didSet {
            logo.layer.cornerRadius = 4.0
            logo.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleLabel.text = titleString
        self.logo.image = logoImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    class func loadFromNib() -> TutorialPageViewController {
        let viewController = TutorialPageViewController(nibName: String(TutorialPageViewController), bundle: nil)
        
        return viewController
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
