//
//  MapViewController.swift
//  Provocco
//
//  Created by ioan on 02.04.2016.
//  Copyright © 2016 pentalog. All rights reserved.
//

import CoreLocation
import MapKit
import UIKit

@objc protocol MapViewControllerDelegate {
    func mapViewCoordinatesReturned(coord: CLLocationCoordinate2D)
}

class CustomPopover : UIView {

    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    var viewMoreButton: UIButton!

    var challenge: Challenge!
    weak var viewController: MapViewController?

    override init(frame: CGRect) {
        super.init(frame: frame)

        let horizPos = CGFloat(16)
        let opac:CGFloat = 0xff

        self.backgroundColor = UIColor(red: opac/255.0, green: opac/255.0, blue: opac/255.0, alpha: 0xd0/255.0)
        self.layer.cornerRadius = 8

        self.titleLabel = UILabel()
        self.titleLabel.frame = CGRect(x: horizPos, y: 8, width: frame.size.width - 2 * horizPos, height: 40)
        self.titleLabel.font = UIFont.systemFontOfSize(14)
        self.titleLabel.autoresizingMask = [.FlexibleWidth, .FlexibleBottomMargin]
        self.addSubview(self.titleLabel)

        self.descriptionLabel = UILabel()
        self.descriptionLabel.numberOfLines = 2
        self.descriptionLabel.font = UIFont.systemFontOfSize(12)
        self.descriptionLabel.numberOfLines = 2
        self.descriptionLabel.frame = CGRect(x: horizPos, y: 56, width: frame.size.width - 2 * horizPos, height: 40)
        self.descriptionLabel.autoresizingMask = [.FlexibleWidth, .FlexibleBottomMargin]

        self.addSubview(self.descriptionLabel)

        self.viewMoreButton = UIButton(type: UIButtonType.System)
        self.viewMoreButton.frame = CGRect(x: horizPos, y: 8+40+8+40+8, width: frame.size.width - 2 * horizPos, height: 40)
        self.viewMoreButton.setTitle("View more", forState: .Normal)
        self.viewMoreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        self.viewMoreButton.autoresizingMask = [.FlexibleWidth, .FlexibleBottomMargin]
        self.viewMoreButton.addTarget(self, action: "viewMoreClicked", forControlEvents: .TouchUpInside)
        self.addSubview(self.viewMoreButton)
    }

    func viewMoreClicked() {
        let previewChallenge = PreviewChallengeViewController.loadFromNib()
        previewChallenge.selectedChallenge = self.challenge

        self.viewController?.navigationController?.pushViewController(previewChallenge, animated: true)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CreationCallout : UIView {
//    var locationLabel: UILabel!
    var confirmButton: UIButton!
    weak var viewController: MapViewController?
    weak var annotation: MKAnnotation?

    override init(frame: CGRect) {
        super.init(frame: frame)

        let horizPos = CGFloat(16)
        let opac:CGFloat = 0xff

        self.backgroundColor = UIColor(red: opac/255.0, green: opac/255.0, blue: opac/255.0, alpha: 0xd0/255.0)
        self.layer.cornerRadius = 8

//        self.locationLabel = UILabel()
//        self.locationLabel.frame = CGRect(x: horizPos, y: 8, width: frame.size.width - 2 * horizPos, height: 40)
//        self.locationLabel.autoresizingMask = [.FlexibleWidth, .FlexibleBottomMargin]
//        self.addSubview(self.locationLabel)

        self.confirmButton = UIButton(type: UIButtonType.System)
        self.confirmButton.frame = CGRect(x: horizPos, y: 8, width: frame.size.width - 2 * horizPos, height: 40)
        self.confirmButton.setTitle("Add location", forState: .Normal)
//        self.confirmButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        self.confirmButton.autoresizingMask = [.FlexibleWidth, .FlexibleBottomMargin]
        self.confirmButton.addTarget(self, action: "confirmClicked", forControlEvents: .TouchUpInside)
        self.addSubview(self.confirmButton)

    }

    func confirmClicked() {
        if self.annotation != nil {
            self.viewController?.delegate?.mapViewCoordinatesReturned(self.annotation!.coordinate)
        }
        self.viewController?.navigationController?.popViewControllerAnimated(true)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CustomPinAnnotationView : MKPinAnnotationView
{
    var callout: UIView!
    var isForNew = false
    weak var viewController: MapViewController?
    weak var challenge: Challenge?

    func initDescriptionCallout() {
        self.callout = CustomPopover(frame: CGRect(x: 0, y: 0, width: 192, height: 40 * 3 + 4 * 8))
        (self.callout as! CustomPopover).viewController = self.viewController
        (self.callout as! CustomPopover).challenge = self.challenge
    }

    func initNewCallout() {
        self.callout = CreationCallout(frame: CGRect(x: 0, y: 0, width: 128, height: 40 * 1 + 2 * 8))
        (self.callout as! CreationCallout).annotation = self.annotation
        (self.callout as! CreationCallout).viewController = self.viewController
        self.isForNew = true
    }

    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if let hitView = super.hitTest(point, withEvent: event) {
            return hitView
        }
        if self.selected {
            let pointInCalloutView = self.convertPoint(point, toView: self.callout)
            return self.callout.hitTest(pointInCalloutView, withEvent: event)
        }
        return nil
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            var calloutViewFrame = self.callout.frame
            calloutViewFrame.origin.x = -(calloutViewFrame.width - self.bounds.width) * 0.5
            calloutViewFrame.origin.y = -calloutViewFrame.height + 15.0
            self.callout.frame = calloutViewFrame;

            if let ann = self.annotation as? StoredAnnotation {
                let challenge = ann.challenge
                let popo = self.callout as! CustomPopover
                popo.challenge = challenge
                popo.titleLabel.text = challenge.title
                popo.descriptionLabel.text = challenge.descr
            }

            self.addSubview(self.callout)
            return
        } else {
            self.callout.removeFromSuperview()
        }

    }
}

// This is used just to check the type
class NewAnnotation : MKPointAnnotation {
}

class StoredAnnotation : MKPointAnnotation {
    private let challenge: Challenge
    init(challenge:Challenge) {
        self.challenge = challenge
    }
}

class MapViewController : UIViewController, MKMapViewDelegate
{
    @IBOutlet var mapView: MKMapView!

    private var mapMoved = false    // used to track edit-mode clicks

    private let newAnnotation = NewAnnotation()
    private var didLoad = false

    var annotations: [StoredAnnotation] = []
    var editable:Bool = false {
        didSet {
            // TODO: show right bar button item
            self.title = self.editable ? "Add challenge on map" : "Map"
        }
    }
    var challenges = [Challenge]() {
        didSet {
            if self.didLoad {
                self.updateChallenges()
            }
        }
    }
    weak var delegate: MapViewControllerDelegate?

    func tapped(taprec:UITapGestureRecognizer)
    {
        if !self.editable {
            return
        }

        let coordinate = self.mapView.convertPoint(taprec.locationInView(self.mapView), toCoordinateFromView: self.mapView)
        self.newAnnotation.coordinate = coordinate
        self.mapView.removeAnnotation(self.newAnnotation)
        self.mapView.addAnnotation(self.newAnnotation)

    }

    private func updateChallenges()
    {
        self.annotations = []
        self.mapView.removeAnnotations(self.annotations)
        for challenge in self.challenges {
            guard challenge.longitude != nil && challenge.latitude != nil else {
                continue
            }
            let annotation = StoredAnnotation(challenge:challenge)
            annotation.coordinate = CLLocationCoordinate2D(latitude: challenge.latitude!, longitude: challenge.longitude!)
            self.annotations.append(annotation)
        }
        self.mapView.addAnnotations(self.annotations)
    }

    override func viewDidLoad() {

        let taprec = UITapGestureRecognizer(target: self, action: "tapped:")
        self.mapView.addGestureRecognizer(taprec)

        self.didLoad = true
        self.updateChallenges()
    }

    class func loadFromNib() -> MapViewController {
        let viewController = MapViewController(nibName: String(MapViewController), bundle: nil)
        
        return viewController
    }

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {

        if let newan = annotation as? NewAnnotation {
            if let pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("newpin") {
                pinView.annotation = newan
                return pinView
            }
            let pinView = CustomPinAnnotationView(annotation: newan, reuseIdentifier: "newpin")
            pinView.viewController = self
            pinView.initNewCallout()
            pinView.animatesDrop = true
            pinView.pinColor = .Purple
            return pinView
        }

        if let pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("pin") {
            pinView.annotation = annotation
            return pinView
        }
        let pinView = CustomPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        pinView.viewController = self
        pinView.initDescriptionCallout()
        pinView.animatesDrop = false
        pinView.canShowCallout = false

        return pinView
    }

    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if let _ = view.annotation as? NewAnnotation {
            return
        }
        self.annotToSelect = nil
        self.mapView.removeAnnotation(newAnnotation)
    }

    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.mapMoved = true
    }

    private weak var annotToSelect: MKAnnotation?
    func delayedSelect() {
        if annotToSelect != nil {
            self.mapView.selectAnnotation(annotToSelect!, animated: true)
        }
    }

    func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
        for annview in views {
            if let annot = annview.annotation as? NewAnnotation {
                NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("delayedSelect"), userInfo: nil, repeats: false)
                self.annotToSelect = annot
                return
            }
        }
    }
}
