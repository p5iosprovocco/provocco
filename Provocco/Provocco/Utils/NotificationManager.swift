//
//  NotificationManager.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/12/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation

/// All the notifications identifiers used in this app
struct NotificationIdentifiers {
    /**
     Changes in the authentification token
     */
    static let AuthTokenDidChangeNotification = "AuthTokenDidChangeNotification"
}

/// NotificationManager (NSNotificationCenter) for FoodAndYou Application
class NotificationManager: NSObject {
    
    
    // -------------------------------------------------------------------------------
    // MARK: - Notification Methods
    
    /**
     Post a NSNotification to inform about changes in authentification token
    */
    class func postAuthTokenDidChangeNotification() {
        NSNotificationCenter.defaultCenter().postNotificationName(NotificationIdentifiers.AuthTokenDidChangeNotification, object: nil)
    }
}