//
//  AppStrings.swift
//  BrushnBarber
//
//  Created by Adrian Zghibarta on 3/29/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

enum AppStrings: String {
    
    // All alert and quick actions button titles
    case Error = "Erreur"
    case Ok = "Ok"
    case Cancel = "Annuler"
    
    // Alerts message strings
    case EmptyFieldsErrorMessage = "S'il vous plaît remplir tous les champs."
}
