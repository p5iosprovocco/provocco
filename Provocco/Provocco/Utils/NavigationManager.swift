//
//  NavigationManager.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 2/5/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

/// All basic operations for navigation into the application
class NavigationManager: NSObject {
    
    // -------------------------------------------------------------------------------
    // MARK: - Proprietes
    
    var window: UIWindow = UIWindow()
    var application: UIApplication?
    var appDelegate: AppDelegate?
    
    static let sharedInstance = NavigationManager()
    
    override init() {
        super.init()
    }
    
    func setRootViewController(viewController: UIViewController) {
        
        self.window.rootViewController = viewController
        self.window.makeKeyAndVisible()
    }
 }
