//
//  UserDefaultStorage.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/20/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

enum UserDefaultSharedStorageKeys : String {
    case tokenKey = "token"
    case refreshTokenKey = "refresh_token"
    case selectedCurrencyKey = "selectedCurrency"
    case deviceTokenKey = "deviceToken"
}

class UserDefaultStorage: NSObject {
    
    // -------------------------------------------------------------------------------
    // MARK: - Proprietes
    
    static let sharedInstance = UserDefaultStorage()
    private let userDefaults = NSUserDefaults.standardUserDefaults()
    
    // -------------------------------------------------------------------------------
    // MARK: - Device Token Methods
    
    func saveDeviceToken(deviceToken: String) {
        userDefaults.setObject(deviceToken, forKey: UserDefaultSharedStorageKeys.deviceTokenKey.rawValue)
    }
    
    func getDeviceToken() -> String? {
        
        if let token = userDefaults.objectForKey(UserDefaultSharedStorageKeys.deviceTokenKey.rawValue) as? String {
            if !token.isEmpty {
                return token
            }
        }
        
        return nil
    }
    
    // -------------------------------------------------------------------------------
    // MARK: - Token methods
    
    /**
    Save the token and refresh token to NSUserDefaults
    
    - parameter token:    token value
    - parameter refToken: refresh token value
    */
    func saveTokenInformation(token token: NSString, refreshToken refToken: String) {
        userDefaults.setObject(token, forKey: UserDefaultSharedStorageKeys.tokenKey.rawValue)
        userDefaults.setObject(refToken, forKey: UserDefaultSharedStorageKeys.refreshTokenKey.rawValue)
        
        //Post a notification to inform all subscribed object about token changes
        NotificationManager.postAuthTokenDidChangeNotification()
    }
    
    /**
     Delete the token and refresh token from the NSUserDefaults storage
     */
    func deleteTokenInformation() {
        userDefaults.setObject("", forKey: UserDefaultSharedStorageKeys.tokenKey.rawValue)
        userDefaults.setObject("", forKey: UserDefaultSharedStorageKeys.refreshTokenKey.rawValue)
        
        //Post a notification to inform all subscribed object about token changes
        NotificationManager.postAuthTokenDidChangeNotification()
    }
    
    
    /**
     Get the token value from the NSUserDefaults
     
     - returns: token value
     */
    func getToken() -> String? {
        if let token = userDefaults.objectForKey(UserDefaultSharedStorageKeys.tokenKey.rawValue) as? String {
            if !token.isEmpty {
                return token
            }
        }
        
        return nil
    }
    
    /**
     Get the Refresh token from the NSUserDefaults
     
     - returns: refresh token
     */
    func getRefreshToken() -> String? {
        if let refreshToken = userDefaults.objectForKey(UserDefaultSharedStorageKeys.refreshTokenKey.rawValue) as? String {
            if !refreshToken.isEmpty {
                return refreshToken
            }
        }
        
        return nil
    }
}